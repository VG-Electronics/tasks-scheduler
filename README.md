<h1>Alternative Tasks Scheduler for Laravel</h1>

<h2>Introduction</h2>

The very first version of an alternative tasks scheduler for CRON tasks made in Laravel. **It's not suitable for use!**<br>
I've made a simple scheduler for my private projects (you can see how to make it [in my YouTube tutorial](https://www.youtube.com/watch?v=i1W2rTV5-4E)) and now I've decided to make it once again as a standalone, extended package for developers.

<h2>When you need it?</h2>

The main reason why you need an alternative for the default Laravel scheduler is **the PHP proc_open function disabled on your server**.<br>This function is required by the Symfony\Component\Process\Process class and this class is used by default scheduler. Simply put, if you're not able to enable proc_open function, you need to find a workaround.

My scheduler is very similar to the default one. You'll be able to run your tasks:
- every minute / thirty minutes / hour
- at 8 o'clock everyday / only on weekdays / only on weekends
- at specific day at specific time


<h3>Examples:</h3>

These examples are related to current scheduler you can make using my tutorial and not to current state of the package. Future package will be even more extended and similar to Laravel scheduler.


```
ScheduledTask::schedule( DummyTask::class )->dailyAt(7, 40) // everyday at 7:40
ScheduledTask::schedule( DummyTask::class )->staticTime(7, 8, 0) // on sunday at 8:00
ScheduledTask::schedule( DummyTask::class )->interval(30) // every 30 minutes
ScheduledTask::schedule( DummyTask::class )->weekdaysAt(12,20) // monday - friday at 12:20
```


<h2>Coming soon...</h2>

<?php
namespace VG\TasksScheduler\ScheduledTask;

use App\Helpers\CarbonHelper;

use Illuminate\Console\Command;

class ScheduledTask{

    const Monday = 1;
    const Tuesday = 2;
    const Wednesday = 3;
    const Thursday = 4;
    const Friday = 5;
    const Saturday = 6;
    const Sunday = 7;

    private $staticTimes = [];
    private $intervalMinutes = null;

    public function __construct( $command ){
        $this->commandName = $command;
    }

    public static function schedule( $command ){
        return new $command();
    }

    public function isTimeToExecute(){
        if( $this->intervalMinutes ){
            $status = ( CarbonHelper::getMinutesSinceStartOfDay() % $this->intervalMinutes == 0 );
        }else{
            CarbonHelper::getDayOfWeekAndHourAndMinute($day, $hour, $minute);

            $status = false;
            foreach( $this->staticTimes as $time ){
                if( $day == $time['day'] && $hour == $time['hour'] && $minute == $time['minute'] ){
                    $status = true;
                    break;
                }
            }
        }

        return $status ? true : false;
    }

    public function every( $minutes ){
        $this->setIntervalMinutes($minutes);

        return $this;
    }

    public function everyMinute(){
        return $this->every(1);
    }

    public function everyFiveMinutes(){
        return $this->every(5);
    }

    public function everyTenMinutes(){
        return $this->every(10);
    }

    public function everyFifteenMinutes(){
        return $this->every(15);
    }

    public function everyTwentyMinutes(){
        return $this->every(20);
    }

    public function everyThirtyMinutes(){
        return $this->every(30);
    }

    public function everyHour(){
        return $this->every(60);
    }
    
    public function everyTwoHours(){
        return $this->every(60 * 2);
    }

    public function everyThreeHours(){
        return $this->every(60 * 3);
    }

    public function everyFourHours(){
        return $this->every(60 * 4);
    }

    public function everySixHours(){
        return $this->every(60 * 6);
    }

    public function everyEightHours(){
        return $this->every(60 * 8);
    }

    public function everyTwelveHours(){
        return $this->every(60 * 12);
    }

    public function staticTime($day, $hour, $minute = 0 ){
        $this->validateStaticTime($day, $hour, $minute);

        $this->setStaticTime($day, $hour, $minute);

        return $this;
    }

    public function weekly(){
        return $this->sundayAt(0, 0);
    }

    public function daily(){
        return $this->dailyAt(0, 0);
    }

    public function dailyAt( $hour, $minute = 0 ){
        $this->validateStaticTime(null, $hour, $minute);

        for( $day = 1; $day <= 7; $day++ )
            $this->setStaticTime($day, $hour, $minute);

        return $this;
    }

    public function weekdaysAt( $hour, $minute = 0 ){
        $this->validateStaticTime(null, $hour, $minute);

        for( $day = 1; $day <= 5; $day++ )
            $this->setStaticTime($day, $hour, $minute);

        return $this;
    }

    public function weekendsAt( $hour, $minute = 0 ){
        $this->validateStaticTime(null, $hour, $minute);

        for( $day = 6; $day <= 7; $day++ )
            $this->setStaticTime($day, $hour, $minute);

        return $this;
    }

    public function mondayAt( $hour, $minute = 0 ){
        return $this->setStaticForDayOfWeek( self::Monday, $hour, $minute );
    }

    public function tuesdayAt( $hour, $minute = 0 ){
        return $this->setStaticForDayOfWeek( self::Tuesday, $hour, $minute );
    }

    public function wednesdayAt( $hour, $minute = 0 ){
        return $this->setStaticForDayOfWeek( self::Wednesday, $hour, $minute );
    }

    public function thursdayAt( $hour, $minute = 0 ){
        return $this->setStaticForDayOfWeek( self::Thursday, $hour, $minute );
    }

    public function fridayAt( $hour, $minute = 0 ){
        return $this->setStaticForDayOfWeek( self::Friday, $hour, $minute );
    }

    public function saturdayAt( $hour, $minute = 0 ){
        return $this->setStaticForDayOfWeek( self::Saturday, $hour, $minute );
    }

    public function sundayAt( $hour, $minute = 0 ){
        return $this->setStaticForDayOfWeek( self::Sunday, $hour, $minute );
    }

    private function setStaticForDayOfWeek( $day, $hour, $minute ){
        $this->validateStaticTime(null, $hour, $minute);

        $this->setStaticTime( $day, $hour, $minute );

        return $this;
    }

    private function setIntervalMinutes( $minutes ){
        $this->intervalMinutes = $minutes;
    }

    private function setStaticTime( $day, $hour, $minute ){
        $this->intervalMinutes = null;
        $this->staticTimes[] = compact('day', 'hour', 'minute');
    }

    private function validateStaticTime( $day = null, $hour, $minute ){

        if( $day !== null && ($day < 1 || $day > 7) )
            throw new \Exception('Day must be a number between 1 and 7.');

        if( $hour < 0 || $hour > 23 )
            throw new \Exception('Hour must be a number between 0 and 23.');
        
        if( $minute < 0 || $minute > 59 )
            throw new \Exception('Minute must be a number between 0 and 59.');
    }
}
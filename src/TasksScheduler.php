<?php
namespace VG\TasksScheduler;

use App\Console\Commands\WeeklyReportForEmployer;

class TasksScheduler{

    private $commands;

    public function __construct(){
        $this->commands = [

        ];
    }

    public static function run(){
        return ( new self() )->executeTasksForThisTime();
    }

    public function executeTasksForThisTime(){
        $tasksToExecute = [];

        foreach( $this->commands as $command ){
            if( $command->isTimeToExecute() )
                $tasksToExecute[] = $command;
        }

        foreach( $tasksToExecute as $task ){
            try{
                $task->handle();
            }catch(\Exception $e){
                continue;
            }
        }
    }

    public function schedule( $command ){
        if( in_array($this->schedules, $command) )
            throw new \Exception('Command '.$command.' is already scheduled.');

        $task = new ScheduledTask( $command );

        $this->schedules[ $command ] = $task;

        return $task;
    }
}

$scheduler = new TasksScheduler();
$scheduler->schedule( WeeklyReport::class )->everyMinute();
$scheduler->schedule( WeeklyReport::class )->dailyAt(8, 30);
$scheduler->schedule( WeeklyReport::class )->weekdaysAt(12);

$scheduler->run();

$scheduler->setCommands([
    $scheduler->schedule( WeeklyReport::class )->everyMinute(),
]);

